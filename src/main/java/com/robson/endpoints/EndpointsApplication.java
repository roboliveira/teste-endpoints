package com.robson.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@RestController
public class EndpointsApplication {

	@RequestMapping("/reliable")
	public String reliable(){
		return "Retorno do serviço consumido";
	}

	@RequestMapping("/requestComErro")

	public String requestComErro(){

		Integer a = 1/0;
		return "Erro";
	}


	@RequestMapping("/requestComDelay")
	public String requestComDelay(){
		try {
			TimeUnit.SECONDS.sleep(1    );
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return "Retorno com delay";
	}

	@RequestMapping("/requestInstavel")
	public String requestInstavel(){
		Random random = new Random();
		int tipo = random.ints(1, 4).findFirst().getAsInt();
		String msg = null;
		System.out.println(tipo);
		switch (tipo){
			case 1: //segue normalmente
				msg = "Sem erros";
				break;
			case 2: // erro durante a requisição
				int a = 1/0;
				break;
			case 3: // delay durante a requisição
				try {
					TimeUnit.SECONDS.sleep(1    );
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				msg = "Com delay";
				break;
		}
		return msg;
	}

	public static void main(String[] args) {
		SpringApplication.run(EndpointsApplication.class, args);
	}

}
